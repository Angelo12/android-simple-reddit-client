package com.java1.angelogomez.java1_project2;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by AngeloGomez on 5/2/15.
 */
public class RemoteSubSearch extends AsyncTask<String, Void, String> {

    String mURL;
    Context mContext;
    //CustomAdapter cAdapter;
    //GridView theGV;
    View parent;

    public RemoteSubSearch(String mURL, Context mContext) {
        this.mURL = mURL;
        this.mContext = mContext;
        //this.theGV = mGV;
        //cAdapter = new CustomAdapter(mContext);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        parent = ((Activity)mContext).getWindow().getDecorView().findViewById(android.R.id.content);

        //final Button searchTab = (Button) parent.findViewById(R.id.searchTab);

        //searchTab.setEnabled(false);
    }

    @Override
    protected String doInBackground(String... params) {

        try{
            URL url = new URL(mURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            //connection.setRequestProperty("Accept", "application/json");

            try {
                connection.connect();

                InputStream is = connection.getInputStream();


                String data = IOUtils.toString(is);
                is.close();

                return data;

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                connection.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        ArrayList<Subreddit> posts = new ArrayList<>();

        try {

            //Log.i("xData", );
            if(s == null ) {
                Toast.makeText(mContext, "No Subreddits found. Try again!", Toast.LENGTH_SHORT).show();
                return;
            }
            JSONObject outObj = new JSONObject(s);
            JSONObject data = outObj.getJSONObject("data");
            JSONArray children = data.getJSONArray("children");





            for(int i = 0; i < children.length(); i++) {
                JSONObject currentPost = children.getJSONObject(i).getJSONObject("data");
                Log.i("Current Post", currentPost.toString(1));
                String url = currentPost.getString("url");
                String title = currentPost.getString("title");
                String subs = currentPost.getString("subscribers");


                Subreddit newPost = new Subreddit(url,title,subs+ " subscribers");
                posts.add(newPost);
            }




        } catch(JSONException e) {
            e.printStackTrace();
        } finally {

            PostsAdapter pa = new PostsAdapter(null,mContext, false);
            pa.rSubs = posts;
            ListView lv = (ListView) parent.findViewById(R.id.listView);
            lv.setAdapter(pa);

            Button btn = (Button) parent.findViewById(R.id.searchButton);
            btn.setEnabled(true);

        }
    }


}