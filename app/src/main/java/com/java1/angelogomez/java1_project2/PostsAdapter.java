package com.java1.angelogomez.java1_project2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.image.SmartImage;
import com.loopj.android.image.SmartImageView;

import java.util.ArrayList;

/**
 * Created by AngeloGomez on 5/2/15.
 */
public class PostsAdapter extends BaseAdapter {

    ArrayList<Post> rPosts;
    ArrayList<Subreddit> rSubs;
    Context mContext;
    Boolean isPost = true;


    public PostsAdapter(ArrayList<Post> rPosts, Context mContext, Boolean isPost) {
        this.rPosts = rPosts;
        this.mContext = mContext;
        this.isPost = isPost;
        this.rSubs = null;
    }


    static class PostViewHolder {
        public SmartImageView mIv;
        public TextView mTitle;
        public TextView mUsername;
        public TextView mDetails;
        public TextView mComments;
        public LinearLayout mCell;
        public ImageView mCommentView;

        public PostViewHolder(View v) {

            this.mIv = (SmartImageView) v.findViewById(R.id.imageView);
            this.mTitle = (TextView) v.findViewById(R.id.tvTitle);
            this.mUsername = (TextView) v.findViewById(R.id.tvUsername);
            this.mDetails = (TextView) v.findViewById(R.id.tvDetails);
            this.mComments = (TextView) v.findViewById(R.id.tvComments);
            this.mCell = (LinearLayout) v.findViewById(R.id.containerCell);
            this.mCommentView = (ImageView) v.findViewById(R.id.commentView);

        }
    }

    static class SubViewHolder {
       // public SmartImageView mIv;
        public TextView mTitle;
        public TextView mUsername;
        public TextView mDetails;
        //public TextView mComments;
        public LinearLayout mCell;
        //public ImageView mCommentView;

        public SubViewHolder(View v) {

            //this.mIv = (SmartImageView) v.findViewById(R.id.imageView);
            this.mTitle = (TextView) v.findViewById(R.id.tvTitle);
            this.mUsername = (TextView) v.findViewById(R.id.tvUsername);
            this.mDetails = (TextView) v.findViewById(R.id.tvDetails);
            //this.mComments = (TextView) v.findViewById(R.id.tvComments);
            this.mCell = (LinearLayout) v.findViewById(R.id.containerCell);
            //this.mCommentView = (ImageView) v.findViewById(R.id.commentView);

        }
    }



    @Override
    public int getCount() {
        if (isPost) {
            return rPosts.size();
        } else {
            return rSubs.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (rPosts.size() > 0 && position < rPosts.size() && position >= 0 && isPost)
            return rPosts.get(position);
        else if (rSubs.size() > 0  && position < rSubs.size() && position >= 0)
            return rSubs.get(position);

        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (isPost) {
            return doPostView(position, convertView, parent);
        } else {
            return doSubView(position, convertView, parent);
        }
    }

    private View doSubView(int position, View convertView, ViewGroup parent) {

        SubViewHolder holder;

        //ImageView customView;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.reddit_subreddits, parent, false);

            holder = new SubViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (SubViewHolder) convertView.getTag();
        }

        final Subreddit currentPost = rSubs.get(position);

        holder.mTitle.setText(currentPost.mUrl);
        holder.mUsername.setText(currentPost.mTitle);
        holder.mDetails.setText(currentPost.mSubscribers);
       // holder.mComments.setText(currentPost.mComments);
       // holder.mIv.setImageUrl(currentPost.mThumbnailUrl);




        holder.mCell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Open Url
                //WebView wv = new WebView(mContext);

                // wv.loadUrl(currentPost.mPostUrl);
                String subRedditUrl = "http://api.reddit.com" + currentPost.mUrl;
                RemoteData rd = new RemoteData(subRedditUrl,mContext);
                rd.execute();
            }
        });

        return convertView;
    }


    private View doPostView(int position, View convertView, ViewGroup parent) {

        PostViewHolder holder;

        //ImageView customView;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.reddit_post, parent, false);

            holder = new PostViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (PostViewHolder) convertView.getTag();
        }

        final Post currentPost = rPosts.get(position);

        holder.mTitle.setText(currentPost.mTitle);
        holder.mDetails.setText(currentPost.mDetails);
        holder.mUsername.setText(currentPost.mUser);
        holder.mComments.setText(currentPost.mComments);

        if(currentPost.mThumbnailUrl.length() > 0)
            holder.mIv.setImageUrl(currentPost.mThumbnailUrl);
        else
            holder.mIv.setVisibility(View.GONE);





        holder.mCell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Open Url
                //WebView wv = new WebView(mContext);

                // wv.loadUrl(currentPost.mPostUrl);

                Uri uri = Uri.parse(currentPost.mPostUrl);

                Intent in = new Intent(Intent.ACTION_VIEW, uri);
                mContext.startActivity(in);

            }
        });

        holder.mCommentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(currentPost.mPermaLink);

                Intent in = new Intent(Intent.ACTION_VIEW, uri);
                mContext.startActivity(in);
            }
        });

        return convertView;
    }
}
