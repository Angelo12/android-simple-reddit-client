package com.java1.angelogomez.java1_project2;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;



public class MainActivity extends Activity {


    String apiUrl = "http://api.reddit.com/";
    String apiSearchUrl = "http://api.reddit.com/subreddits/search?q=";
    Context mContext;

    Button searchButton;
    EditText searchText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;

        searchButton = (Button) findViewById(R.id.searchButton);
        searchText = (EditText) findViewById(R.id.editText);

        LaunchFrontPage();

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String str = searchText.getText().toString();

                if(str.isEmpty()) {
                    LaunchFrontPage();
                } else {
                    SearchForSub(str);
                }
            }
        });

    }

    private void LaunchFrontPage() {

        ConnectivityManager mgr = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo info = mgr.getActiveNetworkInfo();



        if(info != null) {

            if(info.isConnected()) {

                RemoteData rd = new RemoteData(apiUrl,mContext);
                rd.execute();

                searchButton.setEnabled(false);

            } else {
                // Toast.makeText(this, "No Internet Connection", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_LONG).show();
            searchButton.setEnabled(false);
        }
    }

    private void SearchForSub(String s) {

        ConnectivityManager mgr = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo info = mgr.getActiveNetworkInfo();



        if(info != null) {

            if(info.isConnected()) {

                String url = apiSearchUrl;
                String search = "";

                try {
                    search = URLEncoder.encode(s, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                url += search;

                RemoteSubSearch rd = new RemoteSubSearch(url,mContext);
                rd.execute();

                searchButton.setEnabled(false);

            } else {
                // Toast.makeText(this, "No Internet Connection", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_LONG).show();
            searchButton.setEnabled(false);
        }
    }
}








