package com.java1.angelogomez.java1_project2;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.image.SmartImageView;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by AngeloGomez on 5/2/15.
 */
public class RemoteData extends AsyncTask<String, Void, String> {

    String mURL;
    Context mContext;
    //CustomAdapter cAdapter;
    //GridView theGV;
    View parent;

    public RemoteData(String mURL, Context mContext) {
        this.mURL = mURL;
        this.mContext = mContext;
        //this.theGV = mGV;
        //cAdapter = new CustomAdapter(mContext);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        parent = ((Activity)mContext).getWindow().getDecorView().findViewById(android.R.id.content);

        //final Button searchTab = (Button) parent.findViewById(R.id.searchTab);

        //searchTab.setEnabled(false);
    }

    @Override
    protected String doInBackground(String... params) {

        try{
            URL url = new URL(mURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            //connection.setRequestProperty("Accept", "application/json");

            try {
                connection.connect();

                InputStream is = connection.getInputStream();


                String data = IOUtils.toString(is);
                is.close();

                return data;

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                connection.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        ArrayList<Post> posts = new ArrayList<>();

        try {

            //Log.i("xData", );
            if(s == null ) {
                Toast.makeText(mContext, "No Subreddits found. Try again!", Toast.LENGTH_SHORT).show();
                return;
            }
            JSONObject outObj = new JSONObject(s);
            JSONObject data = outObj.getJSONObject("data");
            JSONArray  children = data.getJSONArray("children");

            for(int i = 0; i < children.length(); i++) {
                JSONObject currentPost = children.getJSONObject(i).getJSONObject("data");
                Log.i("Current Post", currentPost.toString(1));
                String title = currentPost.getString("title");
                String user = currentPost.getString("author");
                String comments = currentPost.getString("num_comments");

                //Details
                String sub = currentPost.getString("subreddit");
                String score = currentPost.getString("score");
                String domain = currentPost.getString("domain");
                domain = domainCheck(domain);
                //String createdOn = currentPost.getString("created_utc");
                //createdOn = getCreationTime(createdOn);

                String thumbnail = "https://36.media.tumblr.com/cc11abfd652f44fd2c8f945e8da218ad/tumblr_nnpsaw7Y5y1tbsuuao1_400.png";

                if(currentPost.has("thumbnail")) {
                    thumbnail = currentPost.getString("thumbnail");
                } else {
                    thumbnail = "";
                }

                String Details = sub + " • " + score + " • " + domain;// + " • " + createdOn;

                Post newPost = new Post(title,comments,Details,user,thumbnail, currentPost.getString("url"), "http://reddit.com" + currentPost.getString("permalink"));
                posts.add(newPost);
            }




        } catch(JSONException e) {
            e.printStackTrace();
        } finally {

            PostsAdapter pa = new PostsAdapter(posts,mContext, true);
            ListView lv = (ListView) parent.findViewById(R.id.listView);
            lv.setAdapter(pa);

            Button btn = (Button) parent.findViewById(R.id.searchButton);
            btn.setEnabled(true);

        }
    }

    private String domainCheck(String s) {
        String re = s;

        if(re.contains("self")) {
            re = "self";
        } else if (re.contains("imgur")){
          re = "imgur";
        }  else if (re.contains("tumblr")) {
            re = "tumblr";
        } else if (re.contains("youtube")) {
            re = "youtube";
        } else if (re.contains(".com")) {
            re.replace(".com", "");
        }



        return re;
    }

}