package com.java1.angelogomez.java1_project2;

/**
 * Created by AngeloGomez on 5/2/15.
 */
public class Post {

    String mTitle;
    String mComments;
    String mDetails;
    String mUser;
    String mThumbnailUrl;
    String mPostUrl;
    String mPermaLink;

    public Post(String mTitle, String mComments, String mDetails, String mUser, String mThumbnailUrl, String mPostUrl, String mPermaLink) {
        this.mTitle = mTitle;
        this.mComments = mComments;
        this.mDetails = mDetails;
        this.mUser = mUser;
        this.mThumbnailUrl = mThumbnailUrl;
        this.mPostUrl = mPostUrl;
        this.mPermaLink = mPermaLink;
    }

    public Post(Post p) {
        this.mTitle = p.mTitle;
        this.mComments = p.mComments;
        this.mDetails =         p.mDetails;
        this.mUser =            p.mUser;
        this.mThumbnailUrl =    p.mThumbnailUrl;
        this.mPostUrl =         p.mPostUrl;
    }
}
